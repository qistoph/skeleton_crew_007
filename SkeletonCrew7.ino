#include <LiquidCrystal.h> // https://www.arduino.cc/en/Reference/LiquidCrystal
#include <IRremote.h> // https://github.com/shirriff/Arduino-IRremote.git

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

const int btnPin = 7;

const int backlightPin = 13;

#define PIEZO_PIN  (9)

const int RECV_PIN = 8;
IRrecv irrecv(RECV_PIN);
decode_results results;

byte skull[8] = {
  0b01110,
  0b11111,
  0b10101,
  0b11111,
  0b11011,
  0b01110,
  0b01010,
  0b01110
};

void setup() {
  Serial.begin(115200);
  Serial.println("USBzYXlzIGhpIQo=");
  Serial.println("aHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1HOUx4VVFMX1VjZwo=");
  
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  lcd.createChar(1, skull);

  pinMode(btnPin, INPUT_PULLUP);
  pinMode(backlightPin, OUTPUT);

  irrecv.enableIRIn();
  //irrecv.blink13(true);
  
  backlightOn();

  jamesbond_setup();
}

void backlightOn() {
  digitalWrite(backlightPin, LOW);
}

void backlightOff() {
  digitalWrite(backlightPin, HIGH);
}

void showSkeletonCrew() {
  // 0123456789012345
  //  ## Skeleton ##
  //   ## Crew 7 ##
  lcd.home();
  lcd.print(" \x01\x01 Skeleton \x01\x01 ");
  lcd.setCursor(0, 1);
  lcd.print(" \x01\x01 Crew 007 \x01\x01 ");
}

void initNamen() {
  //
  lcd.setCursor(0, 0);
  //         0123456789012345678901234567890123456789
  lcd.print("Esmee,Daniel,Niels,Christian,Chris,");
  lcd.setCursor(0, 1);
  //         0123456789012345678901234567890123456789
  lcd.print("Rob,Marcel,Charlotte,Michiel,Gert,Bas,");
}

unsigned long nextStateAt = 0;
unsigned long nextSubStateAt = 0;

byte subState = 0;

byte state = 0;
// 0 - display logo
// 1 - blink logo
// 2 - init namen
// 3 - scroll namen

unsigned long toneTimeout = 0;

void loop() {
  switch(state) {
    case 0: // start
      showSkeletonCrew();
      ++state;
      nextStateAt = millis() + 5000; // 5 sec show logo
      break;
    case 1: // blink logo
      if(millis() > nextSubStateAt) {
        nextSubStateAt = millis() + 300;
        if(subState) {
          lcd.noDisplay();
        } else {
          lcd.display();
        }
        subState = !subState;
      }
      if(millis() > nextStateAt) {
        lcd.display();
        ++state;
      }
      break;
    case 2: // init namen
      initNamen();
      ++state;
      nextSubStateAt = millis() + 500;
      nextStateAt = millis() + 10000; // 10 sec scroll namen
      break;
    case 3: // scroll namen
      if(millis() > nextSubStateAt) {
        nextSubStateAt = millis() + 200;
        lcd.scrollDisplayLeft();
      }
      if(millis() > nextStateAt) {
        state = 0;
        backlightOff();
      }
      break;
  }

  if (irrecv.decode(&results)){
    Serial.println(results.value, HEX);
    irrecv.resume();
  }
  
  if(digitalRead(btnPin) == LOW) {
    if(millis() > toneTimeout) {
      toneTimeout = millis() + 100;
      playMusicAndContinue();
    }
  }
}

void playMusicAndContinue() {
  showSkeletonCrew();
  nextStateAt = 0; // To start blinking in the whileSoundInterval
  jamesbond_loop();
  state = 0;
}

void whileSoundInterval() {
  // Abuse nextState and nextStateAt here, to save memory
  if(millis() > nextStateAt) {
    if(state) {
      backlightOn();
      state = 0;
    } else {
      backlightOff();
      state = 1;
    }
    nextStateAt = millis() + 250;
  }
}
